﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace oef28._3DocentWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Docent> lijstDocenten = new List<Docent>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnVoegDocent_Click(object sender, RoutedEventArgs e)
        {
            cbSelecteerDocent.Items.Clear();
            if (!string.IsNullOrEmpty(txtDocentToevoegen.Text))
            {
                bool docentAlInLijst = false;
                foreach (var item in lijstDocenten)
                {
                    if (item.Naam.Equals(txtDocentToevoegen.Text))
                    {
                        docentAlInLijst = true;
                    }
                }
                if (!docentAlInLijst)
                {
                    lijstDocenten.Add(new Docent(txtDocentToevoegen.Text));
                }
                else
                {
                    MessageBox.Show("Docent al in lijst.");
                }
                foreach (var item in lijstDocenten)
                {
                    cbSelecteerDocent.Items.Add(item.Naam);
                }
                txtDocentToevoegen.Text = string.Empty;
            } 
            else
            {
                MessageBox.Show("Tekstveld docent toevoegen is leeg.");
            }
        }

        private void cbSelecteerDocent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lsGegevens.Items.Clear();
            lblDocent.Content = string.Empty;
            if(cbSelecteerDocent.SelectedIndex != -1)
            {
                lblDocent.Content = lijstDocenten[cbSelecteerDocent.SelectedIndex].Naam;
                lsGegevens.Items.Add(lijstDocenten[cbSelecteerDocent.SelectedIndex].ToString());
            }
        }
        private void btnAddVak_Click(object sender, RoutedEventArgs e)
        {
            lsGegevens.Items.Clear();
            if (cbSelecteerDocent.SelectedIndex != -1) 
            {
                if (cbSelecteerDocent.SelectedItem.Equals(lijstDocenten[cbSelecteerDocent.SelectedIndex].Naam))
                {
                    if(!string.IsNullOrEmpty(txtLesuren.Text) && !string.IsNullOrEmpty(txtVak.Text))
                    {
                        if(int.TryParse(txtLesuren.Text, out int lesuren))
                        {
                            if(cbLokaal.SelectedIndex != -1)
                            {
                                string[] lokaal = cbLokaal.SelectedItem.ToString().Split(':');
                                lijstDocenten[cbSelecteerDocent.SelectedIndex].Addvak(new Vak(
                                    txtVak.Text,
                                    lesuren,
                                    lokaal[1]
                                    ));
                                lsGegevens.Items.Add(lijstDocenten[cbSelecteerDocent.SelectedIndex].ToString());
                            } else
                            {
                                MessageBox.Show("Geen lokaal geslecteerd.");
                            }
                        } else
                        {
                            MessageBox.Show("Lesuren moeten numeriek zijn");
                        }
                        txtLesuren.Text = string.Empty;
                        txtVak.Text = string.Empty;
                    } else
                    {
                        MessageBox.Show("Alle tekstvedlen in Vakken zijn niet ingevuld.");
                    }
                }   
            } else
            {
                MessageBox.Show("Selecteer eerst een docent.");
            }
        }

        private void btnVerwijderVak_Click(object sender, RoutedEventArgs e)
        {
            lsGegevens.Items.Clear();
            if (cbSelecteerDocent.SelectedIndex != -1)
            {
                if (cbSelecteerDocent.SelectedItem.Equals(lijstDocenten[cbSelecteerDocent.SelectedIndex].Naam))
                {
                    if (!string.IsNullOrEmpty(txtVak.Text))
                    {
                        if (int.TryParse(txtLesuren.Text, out int lesuren))
                        {
                            if (cbLokaal.SelectedIndex != -1)
                            {
                                    string[] lokaal = cbLokaal.SelectedItem.ToString().Split(':');
                                    lijstDocenten[cbSelecteerDocent.SelectedIndex].RemoveVak(new Vak(
                                        txtVak.Text,
                                        lesuren,
                                        lokaal[1]
                                        ));
                                    lsGegevens.Items.Add(lijstDocenten[cbSelecteerDocent.SelectedIndex].ToString());
                            }
                            else
                            {
                                MessageBox.Show("Geen lokaal geslecteerd.");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Lesuren moeten numeriek zijn");
                        }
                        txtLesuren.Text = string.Empty;
                        txtVak.Text = string.Empty;
                    }
                    else
                    {
                        MessageBox.Show("Alle tekstvedlen in Vakken zijn niet ingevuld.");
                    }
                }
            }
            else
            {
                MessageBox.Show("Selecteer eerst een docent.");
            }
        }
    }
    public class Vak
    {
        private string _beschrijving;
        private int _lesuren;
        private string _leslokaal;

        public Vak() { }
        public Vak(string beschrijving, int lesuren, string leslokaal)
        {
            _beschrijving = beschrijving;
            _lesuren = lesuren;
            _leslokaal = leslokaal;
        }
        public override bool Equals(object obj)
        {
            bool isSame = false;
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                isSame = false;
            }
            else
            {
                Vak v = (Vak)obj;
                isSame = v.Beschrijving == Beschrijving;
            }
            return isSame;
        }
        public override string ToString() => $"{Beschrijving} - {Lesuren} - {Leslokaal}";
        public string Beschrijving { get => _beschrijving; set => _beschrijving = value; }
        public int Lesuren { get => _lesuren; set => _lesuren = value; }
        public string Leslokaal { get => _leslokaal; set => _leslokaal = value; }
    }
    class Docent : Vak
    {
        private string _naam;
        private List<Vak> _vakken = new List<Vak>();

        public Docent() { }
        public Docent(string naam)
        {
            _naam = naam;
        }

        public void Addvak(Vak vak)
        {
            _vakken.Add(vak);
        }
        public void RemoveVak(Vak vak)
        {
             _vakken.Remove(vak);
        }
        public override string ToString()
        {
            string res = "";
            foreach (var item in _vakken)
            {
                res += item.ToString() + Environment.NewLine;
            }
            return res;
        }
        public string Naam { get => _naam; set => _naam = value; }
        private List<Vak> Vakken { get => _vakken; set => _vakken = value; }
    }

}
